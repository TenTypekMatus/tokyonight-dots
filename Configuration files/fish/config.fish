set -U fish_greeting
neofetch -d NixOS
starship init fish | source
alias cat=bat
alias ls="exa -lhi --git --icons"

