require("nvimcfg.plugins") -- Plugins
require('nvimcfg.language.lsp') -- LSP server setup
require('nvimcfg.editor.discord') -- Presence
require('nvimcfg.language.cmp') -- Completion setup
require('nvimcfg.language.mason') -- Mason.nvim
require('nvimcfg.language.autopairs') -- Braces
require('nvimcfg.appearance.airline') -- Statusline
require('nvimcfg.appearance.wilder') -- Wilder
require('nvimcfg.appearance.tabs') -- Tabline
require('nvimcfg.appearance.nvimtree') -- File browser
require('nvimcfg.editor.persistence') -- Persistent sessions
require('nvimcfg.editor.keybinds') -- Key bindings to different actions
require('nvimcfg.editor.opts') -- Other options
require('nvimcfg.editor.term') -- Terminal
