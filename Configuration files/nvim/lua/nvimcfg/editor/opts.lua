vim.cmd[[
colorscheme tokyonight-night
set number
set encoding=UTF-8
set noshowmode
set shell=sh
set signcolumn
]]
vim.o.guifont = "Agave Nerd Font:style=Bold:h14"
