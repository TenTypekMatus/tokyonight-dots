--  WARN: Everything must be properly aligned!!!
vim.cmd[[
nnoremap <space>sf :NvimTreeToggle	  <CR>
nnoremap <space>tn :tabnew	  	  <CR>
nnoremap <space>tc :tabclose	  	  <CR>
nnoremap <space>nt :tabnext 	  	  <CR>
nnoremap <space>tt :ToggleTerm dir=window <CR>
nnoremap <space>lt :TroubleToggle 	  <CR>
]]
